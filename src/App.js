import './App.css';
import { HomeView } from './components/Home/HomeView';

function App() {
  return (
    <HomeView />
  );
}

export default App;
