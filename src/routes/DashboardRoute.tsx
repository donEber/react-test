import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { HomeView } from '../components/Home/HomeView';

export const DashboardRoute = () => {
  return (
    <>
      <Switch>
        <Route path="/home" component={HomeView} />
      </Switch>
    </>
  )
}
